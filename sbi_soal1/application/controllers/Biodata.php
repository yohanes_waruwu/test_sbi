<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('M_biodata');
		$this->load->helper('url');

	}

	function index(){
		$data['biodata'] = $this->M_biodata->tampil_data()->result();
		$this->load->view('biodata/index',$data);
		//$this->load->view('biodata/index');
		
	}

	function tambah(){
		$this->load->view('biodata/input');
		
	}

	function tambah_aksi(){
		$id_biodata = $this->input->post('id_biodata');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');

		

		$data = array(
			'id_biodata' => $id_biodata,
			'nama' => $nama,
			'alamat' => $alamat,

		);
		$this->M_biodata->input_data($data,'biodata');
		redirect('Biodata/index');
	}

	function hapus($id_biodata){
		$where = array('id_biodata' => $id_biodata);

		$this->session->set_flashdata('msg-success', 'Successful Deleted Data.');
		$this->M_biodata->hapus_data($where,'biodata');
		redirect('Biodata/index');
	}

	function edit($id_biodata){
		$where = array('id_biodata' => $id_biodata);
		$data['biodata'] = $this->M_biodata->edit_data($where,'biodata')->result();
		$this->load->view('biodata/edit',$data);
		

	}

	function update(){
		$id_biodata = $this->input->post('id_biodata');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');

		$data = array(
			'nama' => $nama,
			'alamat' => $alamat,
		);	

		$where = array(
			'id_biodata' => $id_biodata
		);
		$this->M_biodata->update_data($where,$data,'biodata');
		//return $query->result();
		redirect('Biodata/index');
	}
}