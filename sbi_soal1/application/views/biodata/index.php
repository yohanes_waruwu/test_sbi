<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
</head>
<body>
    <a href="<?php echo base_url('Biodata/tambah'); ?>"><button>Tambah +</button></a><br><br>
    <table style="width: 100%; border: 1px solid black;">
        <thead style="background-color: Yellow">
            <tr>
                <td>No</td>
                <td>Nama</td>
                <td>Alamat</td>
                <td>Aksi</td>
            </tr>
        </thead>
        <tbody>
            <?php 
            $no = 1;
            foreach($biodata as $u){ 
                ?>
                <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $u->nama?></td>
                    <td><?php echo $u->alamat?></td>

                    <td>
                        <center>
                            <a href='<?php echo base_url('Biodata/edit/'.($u->id_biodata)); ?>'>Edit</a>
                            <a href='<?php echo base_url('Biodata/hapus/'.($u->id_biodata)); ?>'onclick="return confirm('Apakah anda yakin ingin menghapus?')">Hapus</a>
                        </center>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>
</html>